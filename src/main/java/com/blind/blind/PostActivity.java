package com.blind.blind;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.location.*;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit.RestAdapter;
import rx.functions.Action1;

public class PostActivity extends AppCompatActivity {
    private double lat;
    private double lon;
    private API api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_actiity

        );



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void showAlert(final View view) {
        final ProgressDialog spinner = new ProgressDialog(view.getContext());
        spinner.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        spinner.setMessage("Broadcasting your words...");
        spinner.show();
        String hashtag_str="";
        //get the post from UI
        EditText editText = (EditText) findViewById(R.id.editText3);
        // raw post is compele posts including hashtags
        final String rawPost = editText.getText().toString();
        //splitting the post to separate hashtags
        String[] splitedPost= rawPost.split(" ");
        if(splitedPost.length>1) {
                Set<String> hashtag = new HashSet<String>();

            for (String word : splitedPost) {
                if (word.startsWith("#"))
                    hashtag.add(word);
            }

            Iterator<String> iterator = hashtag.iterator();

            while (iterator.hasNext()) {
                hashtag_str += iterator.next();

            }
            hashtag_str = hashtag_str.replace('#', ',');
            hashtag_str=hashtag_str.startsWith(",") ? hashtag_str.substring(1) : hashtag_str;
        }


        final String hashtags =hashtag_str;
        Log.d("hashtag", hashtags);
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        locationProvider.getLastKnownLocation()
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                       final String latitude = String.valueOf(location.getLatitude());
                       final String longitude = String.valueOf(location.getLongitude());
                        Geocoder geocoder = new Geocoder(PostActivity.this, Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        final Address obj = addresses.get(0);
                        String add = obj.getAddressLine(0);

                        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.url).build();
                        api = restAdapter.create(API.class);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                SendPostResponse postResponse;
                                // do the thing that takes a long time
                                if (hashtags != "") {
                                    Log.d("status", "with hatag" + hashtags);
                                    postResponse = api.SendPost(MainActivity.user_id, latitude, longitude, rawPost, hashtags,obj.getAddressLine(0),obj.getAddressLine(1),obj.getCountryName());
                                    Log.d("Response", postResponse.submit);
                                } else {
                                    postResponse = api.SendPost(MainActivity.user_id, latitude, longitude, rawPost,obj.getAddressLine(0),obj.getAddressLine(1),obj.getCountryName());
                                    Log.d("Response", postResponse.submit);
                                }
                            }
                        }).start();

                        AlertDialog.Builder Myalert = new AlertDialog.Builder(PostActivity.this);

                        Myalert.setMessage("your post is submitted with latitude " + latitude + "and longitude" + longitude).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
                        spinner.dismiss();
                        Myalert.show();
                    }
                });


                    }





}
