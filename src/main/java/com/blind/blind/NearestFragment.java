package com.blind.blind;

/**
 * Created by EBAD LODHI on 11/8/2015.
 */

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit.RestAdapter;
import rx.functions.Action1;

public class NearestFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
    private ArrayList<NearbyPostData> receivedPosts;
    private SeekBar seekBar;
    int progress = 1;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        final View V = inflater.inflate(R.layout.nearby_fragment, container, false);
        mRecyclerView = (RecyclerView) V.findViewById(R.id.my_recycler_view);
        receivedPosts = new ArrayList<NearbyPostData>();
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NearbyPostAdapter(receivedPosts,getContext());
        mRecyclerView.setAdapter(mAdapter);









        //  mRecyclerView.setHasFixedSize(true);

        final ProgressDialog spinner = new ProgressDialog(getContext());
        spinner.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        spinner.show();
        getNearbyData(1);
        spinner.dismiss();
        swipeRefreshLayout = (SwipeRefreshLayout) V.findViewById(R.id.nearbyswiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                getNearbyData(progress);
                swipeRefreshLayout.setRefreshing(false);
                Log.i("progres",String.valueOf(progress));


            }

        });


        seekBar = (SeekBar) V.findViewById(R.id.seekBar1);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override

            public void onProgressChanged(SeekBar seekBar, final int progresValue, boolean fromUser) {
                NearestFragment.this.progress = progresValue;


        /*        Toast.makeText(getContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT);*/



            }


            @Override

            public void onStartTrackingTouch(SeekBar seekBar) {
                Toast.makeText(getContext(), "Started tracking seekbar", Toast.LENGTH_SHORT);

            }

            @Override

            public void onStopTrackingTouch(SeekBar seekBar) {

                spinner.dismiss();
                Toast.makeText(getContext(), "Now you see post from" + progress + " Kilometer around you", Toast.LENGTH_SHORT).show();

            }

        });



        return V;
    }

    public void getNearbyData(final int radius)
    {

        receivedPosts.clear();

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getContext());
        locationProvider.getLastKnownLocation()
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.url).build();
                        final API api = restAdapter.create(API.class);
                        // NearbyPostResponse[] data ;
                        final String latitude= String.valueOf(location.getLatitude());
                        final String longitude =String.valueOf(location.getLongitude());
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                NearbyPostResponse nearbyPostResponse=api.getNearbyPost(latitude,longitude,String.valueOf(radius*20));

                                if (nearbyPostResponse.data.length>0){
                                    for(int i =0;i<nearbyPostResponse.data.length;i++) {

                                        NearbyPostData nearbyPostData = new NearbyPostData();
                                        nearbyPostData.content = nearbyPostResponse.data[i].content.toString();
                                        nearbyPostData.latitude = nearbyPostResponse.data[i].latitude.toString();
                                        nearbyPostData.longitude = nearbyPostResponse.data[i].longitude.toString();
                                        nearbyPostData.adressline_1=nearbyPostResponse.data[i].adressline_1;
                                        nearbyPostData.adressline_2=nearbyPostResponse.data[i].adressline_2;
                                        nearbyPostData.countryName=nearbyPostResponse.data[i].countryName;
                                        nearbyPostData._id=nearbyPostResponse.data[i]._id.toString();
                                        nearbyPostData.likes=nearbyPostResponse.data[i].likes.toString();
                                        receivedPosts.add(nearbyPostData);


                                        Log.d("AP", nearbyPostResponse.data[i].content.toString());
                                    }


                                    Collections.reverse(receivedPosts);
                                    // postTextArray[0] = str;

                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {

                                            //    Log.d("UI thread", "I am the UI thread");

                                            mAdapter.notifyDataSetChanged();
                                            //  spinner.dismiss();
                                        }
                                    });

                                }

                                else
                                    Log.d("API", "latitude is :" + latitude + " longitude " + longitude);
                            }
                        }).start();
                    }
                });
        return;
    }



}
