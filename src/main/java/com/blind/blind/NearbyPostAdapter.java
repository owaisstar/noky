package com.blind.blind;

/**
 * Created by Ebad Lodhi on 11/13/2015.
 */

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.RestAdapter;

import static com.blind.blind.R.id;
import static com.blind.blind.R.layout;

public class NearbyPostAdapter extends RecyclerView
        .Adapter<NearbyPostAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<DataObject> mDataset;
    private ArrayList<NearbyPostData> postDataset;
    private static MyClickListener myClickListener;
    public String txt;
    Context ctx ;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView label;
        TextView dateTime;
        TextView loc;
        TextView likes_count;
        ImageButton like;
        String _id;

        public DataObjectHolder(final View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(id.content);
            dateTime = (TextView) itemView.findViewById(id.location);
            likes_count=(TextView) itemView.findViewById(id.likes_count);

           // loc = (TextView) itemView.findViewById(id.textView3);
            like = (ImageButton) itemView.findViewById(id.like_btn);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String postId = _id;
                    RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.url).build();
                    final API api = restAdapter.create(API.class);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                          LikeResponse likeResponse=  api.addLike(MainActivity.user_id, _id);
                        }
                    }).start();

                    Log.d("postId",postId);
                }
            });

        }


        @Override
        public void onClick(View v) {
            if (myClickListener != null) {
                myClickListener.onItemClick(getAdapterPosition(), v);
            }

        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        NearbyPostAdapter.myClickListener = myClickListener;


    }

    //    public MyRecyclerViewAdapter(ArrayList<DataObject> myDataset) {
//        mDataset = myDataset;
//    }
    public NearbyPostAdapter(ArrayList<NearbyPostData> myPostDataset,Context ctx) {
        postDataset = myPostDataset;
        this.ctx=ctx;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(layout.card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }



    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {

        NearbyPostData currentData = postDataset.get(position);
        holder.label.setText(currentData.content);
        holder.dateTime.setText(currentData.adressline_1+" "+currentData.adressline_2+" "+currentData.countryName);
        holder.likes_count.setText(currentData.likes);
        holder._id = currentData._id;




    }

    public void addItem(DataObject dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return postDataset.size();
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);

    }


}

