package com.blind.blind;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import retrofit.RestAdapter;

public class HashtagActivity extends AppCompatActivity {

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText Search;
    private ArrayList<NearbyPostData> receivedPosts;
    RecyclerView recyclerView;
    LinearLayoutManager llm;
    HashtagAdapter hashtagAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    String search_keyword="";
    ProgressDialog spinner;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hashtag);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        spinner=new ProgressDialog(this);
        spinner.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        spinner.setMessage("Searching for #"+search_keyword);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.hashtagswiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                search(search_keyword);
                swipeRefreshLayout.setRefreshing(false);


            }

        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.hashtag_menu, menu);
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        mSearchAction = menu.findItem(R.id.action_search);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_search:
                handleMenuSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void handleMenuSearch(){


        recyclerView = (RecyclerView) findViewById(R.id.hashtagrecyclerview);
        recyclerView.setHasFixedSize(true);
        llm = new LinearLayoutManager(getBaseContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        receivedPosts = new ArrayList<NearbyPostData>();
        hashtagAdapter= new HashtagAdapter(receivedPosts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(hashtagAdapter);
        ActionBar action = getSupportActionBar(); //get the actionbar

        if(isSearchOpened){ //test if the search is open

            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(Search.getWindowToken(), 0);

            //add the search icon in the action bar
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_open_search));

            isSearchOpened = false;
        } else { //open the search entry

            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            Search = (EditText)action.getCustomView().findViewById(R.id.Search); //the text editor

            //this is a listener to do a search when the user clicks on search button
            Search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                        search_keyword= Search.getText().toString();

                       Toast.makeText(getBaseContext(),"do search here",Toast.LENGTH_SHORT).show();
                       search(search_keyword);




                        return true;
                    }
                    return false;
                }
            });


            Search.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(Search, InputMethodManager.SHOW_IMPLICIT);


            //add the close icon
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_open_search));

            isSearchOpened = true;
        }
    }

    public void search(final String query)
    {
        spinner.show();
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.url).build();
        final API api = restAdapter.create(API.class);
       // final String search_keyword= Search.getText().toString();

        final HashtagActivity hashtagActivity = new HashtagActivity();
        receivedPosts.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {

                NearbyPostResponse nearbyPostResponse=api.getPostByHashtag(query);

                if (nearbyPostResponse.data.length>0){
                    for(int i =0;i<nearbyPostResponse.data.length;i++) {

                        NearbyPostData nearbyPostData = new NearbyPostData();
                        nearbyPostData.content = nearbyPostResponse.data[i].content.toString();
                        nearbyPostData.latitude = nearbyPostResponse.data[i].latitude.toString();
                        nearbyPostData.longitude = nearbyPostResponse.data[i].longitude.toString();
                        nearbyPostData.likes=nearbyPostResponse.data[i].likes.toString();


                        receivedPosts.add(nearbyPostData);

                        Log.d("AP", nearbyPostResponse.data[i].content.toString());
                    }

                    Collections.reverse(receivedPosts);

                    hashtagActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d("UI thread", "I am the UI thread");
                            hashtagAdapter.notifyDataSetChanged();


                        }
                    });

                }
                else
                {

                    NearbyPostData nearbyPostData = new NearbyPostData();
                    nearbyPostData.content="NO POST FOUND :(";
                    nearbyPostData.longitude="";
                    nearbyPostData.latitude="";
                    nearbyPostData.likes="0";
                    receivedPosts.add(nearbyPostData);
                    hashtagActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d("UI thread", "I am the UI thread");
                            hashtagAdapter.notifyDataSetChanged();


                        }
                    });

                    //hashtagAdapter.notifyDataSetChanged();

                }
                spinner.dismiss();
            }
        }).start();
    }
}

