package com.blind.blind;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import retrofit.RestAdapter;

/**
 * Created by nightjaralam on 2/7/16.
 */
public class SplashActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final DBhelper dBhelper =new DBhelper(SplashActivity.this);

                    RestAdapter restAdapter= new RestAdapter.Builder().setEndpoint(MainActivity.url).build();

                    final API  api = restAdapter.create(API.class);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // user_id=dBhelper.getUserId();
                            if (MainActivity.user_id==null) {
                                RegisterResponse rr = api.register();
                                Log.d("API", rr.id);
                                dBhelper.insertid(rr.id);
                                MainActivity.user_id=dBhelper.getUserId();
                                Log.d("id is ",MainActivity.user_id);
                            }
                            else
                                Log.d("id exist ",MainActivity.user_id);
                        }
                    }).start();
                    Thread.sleep(3000);


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally {
                    Intent i= new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }).start();
    }
}
/*

    setContentView(R.layout.splash_screen);
new Thread(new Runnable() {
@Override
public void run() {
        try {
        Thread.sleep(3000);
        }
        catch (InterruptedException e)
        {
        e.printStackTrace();
        }
        finally {
        Intent i= new Intent(SplashActivity.this,MainActivity.class);
        startActivity(i);

        }
        }
        }).start();*/
