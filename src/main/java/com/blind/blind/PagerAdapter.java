package com.blind.blind;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

/**
 * Created by nightjaralam on 2/17/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

int numOfTabs;
    public PagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs=numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                MypostFragment mypostFragment = new MypostFragment();
                Log.d("position", String.valueOf(position));
                return mypostFragment;
            case 1:
                NearestFragment nearestFragment= new NearestFragment();
                Log.d("position", String.valueOf(position));
                return  nearestFragment;
            case 2:
                favouriteFragment mfavouriteFragment= new favouriteFragment();
                Log.d("position", String.valueOf(position));
                return mfavouriteFragment;
            default:
                Log.d("position", String.valueOf(position));
                return  null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
