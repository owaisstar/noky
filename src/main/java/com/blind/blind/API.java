package com.blind.blind;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Sarfaraz on 29/10/2015.
 */
public interface API {
    @GET("/register")
    RegisterResponse checkRegister(
            @Query("id") String id
    );
    @GET("/post")
    SendPostResponse SendPost(
            @Query("id") String id,
            @Query("lat") String latitude,
            @Query("lon") String longitude,
            @Query("content") String content,
            @Query("hashtag") String hashtags,
            @Query("adr1") String addressline_1,
            @Query("adr2") String addressline_2,
            @Query("country") String countryName

    );
    @GET("/post")
    SendPostResponse SendPost(
            @Query("id") String id,
            @Query("lat") String latitude,
            @Query("lon") String longitude,
            @Query("content") String content,
            @Query("adr1") String addressline_1,
            @Query("adr2") String addressline_2,
            @Query("country") String countryName


    );

    @GET("/nearby")
    NearbyPostResponse getNearbyPost(
            @Query("lat") String latitude,
            @Query("lon") String longitude,
            @Query("radius") String radius

    );

            @GET("/register")
            RegisterResponse register(

            );

    @GET("/getPost")
    NearbyPostResponse getMyPosts(
            @Query("id") String id
    );

    @GET("/getlikedpost")
    NearbyPostResponse getLikedPost(
            @Query("user_id") String id

    );
    @GET("/addlike")
    LikeResponse addLike(
            @Query("liker_id") String liker_id,
            @Query("post_id") String post_id
    );


    @GET("/getPost")
    NearbyPostResponse getPostByHashtag(
            @Query("hashtag") String hashtag
    );

}
