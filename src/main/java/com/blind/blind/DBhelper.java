package com.blind.blind;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nightjaralam on 2/3/16.
 */
public class DBhelper extends SQLiteOpenHelper {

    final static String DB_NAME="BLIND";
    String TABLE_NAME="Account_details";
    //coloumn names

    String USER_ID="user_id";

    public DBhelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String Create_TABLE="CREATE TABLE "+TABLE_NAME+" ("+USER_ID+" TEXT)";
        db.execSQL(Create_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST "+TABLE_NAME);
        onCreate(db);
    }

    public void insertid(String id)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_ID, id);
        db.insert(TABLE_NAME, null, values);
    }

    public String getUserId()
    {
        String user_id;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cur = db.query(TABLE_NAME,new String[]{USER_ID},null,null,null,null,null);
        if(cur!=null)
        cur.moveToFirst();
        if (cur.getCount()>0)
        {
            user_id=cur.getString(0);
            return user_id;
        }
        else
        return null;
    }

}
