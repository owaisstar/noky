package com.blind.blind;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit.RestAdapter;
import rx.functions.Action1;

/**
 * Created by EBAD LODHI on 11/8/2015.
 */
public class MypostFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<NearbyPostData> receivedPosts;
    ProgressDialog spinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        receivedPosts = new ArrayList<NearbyPostData>();
        View V = inflater.inflate(R.layout.mypost_fragment, container, false);
        mRecyclerView = (RecyclerView) V.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NearbyPostAdapter(receivedPosts,getContext());
        mRecyclerView.setAdapter(mAdapter);

        spinner = new ProgressDialog(V.getContext());
        spinner.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        spinner.show();
        getMyPosts();



        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) V.findViewById(R.id.mypostswiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                spinner.show();
                getMyPosts();
                swipeRefreshLayout.setRefreshing(false);

            }
        });





        return V;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d("resume", "in resume");

    }

    public void getMyPosts()
    {   spinner.show();

        receivedPosts.clear();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.url).build();
        final API api = restAdapter.create(API.class);


        new Thread(new Runnable() {
            @Override
            public void run() {


                NearbyPostResponse nearbyPostResponse = api.getMyPosts(MainActivity.user_id);

                if (nearbyPostResponse.data.length > 0) {
                    for (int i = 0; i < nearbyPostResponse.data.length; i++) {

                        NearbyPostData nearbyPostData = new NearbyPostData();
                        nearbyPostData.content = nearbyPostResponse.data[i].content.toString();
                        nearbyPostData.latitude = nearbyPostResponse.data[i].latitude.toString();
                        nearbyPostData.longitude = nearbyPostResponse.data[i].longitude.toString();
                        nearbyPostData.adressline_1=nearbyPostResponse.data[i].adressline_1;
                        nearbyPostData.adressline_2=nearbyPostResponse.data[i].adressline_2;
                        nearbyPostData.countryName=nearbyPostResponse.data[i].countryName;
                        nearbyPostData._id = nearbyPostResponse.data[i]._id.toString();
                        nearbyPostData.likes= nearbyPostResponse.data[i].likes.toString();

                        receivedPosts.add(nearbyPostData);

                        Log.d("AP", nearbyPostResponse.data[i].content.toString());
                    }




                    Collections.reverse(receivedPosts);
                    // postTextArray[0] = str;

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {


                            Log.d("UI thread", "I am the UI thread");

                            mAdapter.notifyDataSetChanged();


                        }
                    });

                } else {
                    NearbyPostData nearbyPostData = new NearbyPostData();
                    nearbyPostData.content="no post yet";
                    nearbyPostData.longitude="0";
                    nearbyPostData.latitude="0";
                    receivedPosts.add(nearbyPostData);

                }
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d("UI thread", "I am the UI thread");

                        mAdapter.notifyDataSetChanged();


                    }
                });


            }
        }).start();
        spinner.dismiss();
        return;
    }
}

