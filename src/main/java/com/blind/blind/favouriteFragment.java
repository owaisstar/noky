package com.blind.blind;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;

import retrofit.RestAdapter;

/**
 * Created by EBAD LODHI on 11/8/2015.
 */
public class favouriteFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";
  /*  private String postTextArray[];
    private String usedId[];
    private  String loc[];

  */
    ProgressDialog spinner;
    private ArrayList<NearbyPostData> receivedPosts;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View V = inflater.inflate(R.layout.liked_fragment, container, false);
        mRecyclerView = (RecyclerView) V.findViewById(R.id.my_recycler_view);
        receivedPosts = new ArrayList<NearbyPostData>();
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NearbyPostAdapter(receivedPosts,getContext());
        mRecyclerView.setAdapter(mAdapter);;
        //initialize spinner
        spinner = new ProgressDialog(getContext());
        spinner.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // gett posts from server
        getLikedPosts();

        //getting swipe refresh layout for liked fragment
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) V.findViewById(R.id.likedpostswiperefresh);

        //setting refresh listener
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLikedPosts();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return V;
    }

    public void getLikedPosts()
    {


        receivedPosts.clear();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(MainActivity.url).build();
        final API api = restAdapter.create(API.class);
       // NearbyPostResponse[] data ;



       spinner.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                NearbyPostResponse nearbyPostResponse=api.getLikedPost(MainActivity.user_id);

                if (nearbyPostResponse.data.length>0){
                    for(int i =0;i<nearbyPostResponse.data.length;i++) {

                        NearbyPostData nearbyPostData = new NearbyPostData();
                        nearbyPostData.content = nearbyPostResponse.data[i].content.toString();
                        nearbyPostData.latitude = nearbyPostResponse.data[i].latitude.toString();
                        nearbyPostData.longitude = nearbyPostResponse.data[i].longitude.toString();
                        nearbyPostData.adressline_1=nearbyPostResponse.data[i].adressline_1;
                        nearbyPostData.adressline_2=nearbyPostResponse.data[i].adressline_2;
                        nearbyPostData.countryName=nearbyPostResponse.data[i].countryName;
                        nearbyPostData._id=nearbyPostResponse.data[i]._id.toString();
                        nearbyPostData.likes=nearbyPostResponse.data[i].likes.toString();


                        receivedPosts.add(nearbyPostData);

                        Log.d("AP", nearbyPostResponse.data[i].content.toString());
                    }


                    Collections.reverse(receivedPosts);


                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d("UI thread", "I am the UI thread");


                            mAdapter.notifyDataSetChanged();
                            spinner.dismiss();
                        }
                    });

                }

                else
                    Log.d("API", "latitude is :  longitude");
                spinner.dismiss();

            }
        }).start();
        return;
    }
}
