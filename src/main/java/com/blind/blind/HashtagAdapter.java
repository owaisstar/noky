package com.blind.blind;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by nightjaralam on 2/14/16.
 */
public class HashtagAdapter extends RecyclerView.Adapter<HashtagAdapter.cardViewHolder> {

    List<NearbyPostData> postDataSet ;
    HashtagAdapter(List<NearbyPostData> list)
    {
        this.postDataSet=list;
    }

    @Override
    public cardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_row,parent,false);
        return new cardViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(cardViewHolder holder, int position) {
        NearbyPostData data =  postDataSet.get(position);
        holder.content.setText(data.content);
        holder.location.setText(data.latitude+" "+data.longitude);
        holder.likes_count.setText(data.likes);

    }

    @Override
    public int getItemCount() {
        return postDataSet.size();
    }

    public  static class cardViewHolder extends RecyclerView.ViewHolder{
        TextView content;
        TextView location;
        TextView likes_count;

        public cardViewHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.content);
            location=(TextView) itemView.findViewById(R.id.location);
            likes_count=(TextView) itemView.findViewById(R.id.likes_count);
        }
    }


}
