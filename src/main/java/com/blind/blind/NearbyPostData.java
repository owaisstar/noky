package com.blind.blind;

/**
 * Created by nightjaralam on 1/7/16.
 */
public class NearbyPostData {
    public String content;

    public String id;

    public String _id;

    public String[] hashtag;

    public String __v;

    public String longitude;

    public String latitude;

    public String adressline_1;

    public String adressline_2;

    public String   countryName;

    public String likes;

 /*   @Override
    public String toString()
    {
        return "ClassPojo [content = "+this.content+", id = "+id+", _id = "+_id+", hashtag = "+hashtag+", __v = "+__v+", longitude = "+longitude+", latitude = "+latitude+"]";
    }*/

}
